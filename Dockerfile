FROM archlinux/base:latest

ADD files/ /

# Update
RUN pacman --noconfirm -Syu

# Install PHP
RUN pacman --noconfirm --needed -S php56 php56-gd php56-intl php56-mcrypt

RUN ln -s /usr/bin/php56 /usr/bin/php

# Clean up
RUN rm -f \
      /var/cache/pacman/pkg/* \
      /var/lib/pacman/sync/* \
      /etc/pacman.d/mirrorlist.pacnew

